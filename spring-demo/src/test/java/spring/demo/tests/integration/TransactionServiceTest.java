package spring.demo.tests.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import spring.demo.dto.CoinDTO;
import spring.demo.dto.OwnershipDTO;
import spring.demo.dto.TransactionDTO;
import spring.demo.dto.UserDTO;
import spring.demo.entities.Transaction;
import spring.demo.services.CoinService;
import spring.demo.services.OwnershipService;
import spring.demo.services.TransactionService;
import spring.demo.services.UserService;
import spring.demo.tests.config.TestJPAConfiguration;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class TransactionServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private CoinService coinService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private OwnershipService ownershipService;

    @Test
    public void testCreate(){
        UserDTO dto1 = new UserDTO.Builder()
                .name("First")
                .email("user@gmail.com")
                .password("sebi")
                .role("ADMIN")
                .create();

        UserDTO dto2 = new UserDTO.Builder()
                .name("Second")
                .email("user@gmail.com")
                .password("sebi")
                .role("USER")
                .create();

        int user1Id=userService.create(dto1);
        int user2Id =userService.create(dto2);

        CoinDTO coin=new CoinDTO.Builder()
                            .mintage(1000)
                            .currency("Dollar")
                            .description("Fine")
                            .faceValue(100)
                            .meltValue(100)
                            .create();

        int coinId=coinService.create(coin);

        OwnershipDTO ownership1=new OwnershipDTO.Builder()
                                        .coinId(coinId)
                                        .personId(user1Id)
                                        .forSale(true)
                                        .visible(true)
                                        .number(5)
                                        .create();
        int ownership1Id=ownershipService.create(ownership1);

        OwnershipDTO ownership2=new OwnershipDTO.Builder()
                                            .coinId(coinId)
                                            .personId(user2Id)
                                            .forSale(true)
                                            .visible(true)
                                            .number(5)
                                            .create();
        int ownership2Id=ownershipService.create(ownership2);

        TransactionDTO transaction=new TransactionDTO.Builder()
                                            .buyerId(user1Id)
                                            .sellerId(user2Id)
                                            .coinId(coinId)
                                            .number(2)
                                            .price(10)
                                            .create();
        int transactionId=transactionService.create(transaction);

        TransactionDTO transactionDTO= transactionService.findById(transactionId);

        assertTrue("Seller Id ", transactionDTO.getSellerId().equals(user2Id));
        assertTrue("Buyer Id ", transactionDTO.getBuyerId().equals(user1Id));
        assertTrue("number ", transactionDTO.getNumber().equals(2));

    }
}
