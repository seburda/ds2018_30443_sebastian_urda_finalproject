package spring.demo.tests.integration;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;
import spring.demo.tests.config.TestJPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void testCreate() {

		List<UserDTO> fromDB = userService.findAll();
		int size=fromDB.size();
        UserDTO dto1 = new UserDTO.Builder()
                .name("First")
                .email("user@gmail.com")
                .password("sebi")
				.role("ADMIN")
                .create();
        
        UserDTO dto2 = new UserDTO.Builder()
                .name("Second")
                .email("user@gmail.com")
                .password("sebi")
				.role("USER")
                .create();

		userService.create(dto1);
		userService.create(dto2);

		fromDB = userService.findAll();

		assertEquals("One entity inserted", fromDB.size(), size + 2);
	}
    
	@Test
	public void testGetByIdSuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .name("First")
                .email("user@gmail.com")
                .password("sebi")
				.role("USER")
                .create();

		int userId = userService.create(dto);
		UserDTO fromDB = userService.findUserById(userId);

		assertTrue("Name ", dto.getName().equals(fromDB.getName()));
		assertTrue("Email ", dto.getEmail().equals(fromDB.getEmail()));
		assertTrue("Password ", dto.getPassword().equals(fromDB.getPassword()));
	}

}
