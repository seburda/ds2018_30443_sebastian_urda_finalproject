package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Ownership;

import java.util.List;

public interface OwnershipRepository extends JpaRepository<Ownership,Integer> {
    public Ownership findById(int id);
    public List<Ownership> findByOwner_Id(int id);
    public Ownership findByOwner_IdAndCoin_Id(Integer ownerId,Integer coinId);
    public List<Ownership> findByOwner_IdAndAndVisible(int id,boolean visbil);

}
