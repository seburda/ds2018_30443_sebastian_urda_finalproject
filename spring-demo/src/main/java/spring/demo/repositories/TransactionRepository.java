package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Transaction;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction,Integer> {
    public List<Transaction> findBySeller_Id(int id);
    public List<Transaction> findByBuyer_Id(int id);
    public List<Transaction> findByCoin_Id(int id);
    public Transaction findById(int id);
}
