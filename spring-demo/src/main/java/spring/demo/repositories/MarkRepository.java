package spring.demo.repositories;

import spring.demo.entities.Mark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarkRepository extends JpaRepository<Mark,Integer> {
        public List<Mark> findByTaker_Id(int id);
}
