package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Coin;


public interface CoinRepository  extends JpaRepository<Coin, Integer> {

    Coin findById(int id);
}
