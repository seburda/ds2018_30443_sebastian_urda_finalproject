package spring.demo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
@Entity
@Table(name="mark")
public class Mark implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private User giver;
    private User taker;
    private Double mark;

    public Mark() {
    }


    public Mark(User giverId, User takerId, Double mark) {
        this.giver = giverId;
        this.taker = takerId;
        this.mark = mark;
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "giver_Id")
    public User getGiver() {
        return giver;
    }

    public void setGiver(User giverId) {
        this.giver = giverId;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "taker_Id")
    public User getTaker() {
        return taker;
    }

    public void setTaker(User takerId) {
        this.taker = takerId;
    }

    @Column(name = "mark", nullable = false)
    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }
}
