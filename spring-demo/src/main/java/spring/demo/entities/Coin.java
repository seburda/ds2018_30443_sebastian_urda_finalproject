package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="coins")
public class Coin implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer mintage;
    private Double faceValue;
    private Double meltValue;
    private String currency;
    private String description;

    public Coin() {
    }

    public Coin(Integer id, Integer mintage, Double faceValue, Double meltValue,String currency,String description) {
        this.id = id;
        this.mintage = mintage;
        this.faceValue = faceValue;
        this.meltValue = meltValue;
        this.currency=currency;
        this.description=description;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "mintage", nullable = false, length = 200)
    public Integer getMintage() {
        return mintage;
    }

    public void setMintage(Integer mintage) {
        this.mintage = mintage;
    }

    @Column(name = "face_value", nullable = false, length = 200)
    public Double getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(double faceValue) {
        this.faceValue = faceValue;
    }

    @Column(name = "melt_value", nullable = false, length = 200)
    public Double getMeltValue() {
        return meltValue;
    }

    public void setMeltValue(double meltValue) {
        this.meltValue = meltValue;
    }

    @Column(name = "currency", nullable = false, length = 200)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "description", nullable = false, length = 200)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
