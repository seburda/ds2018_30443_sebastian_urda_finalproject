package spring.demo.dto;

public class CoinDTO {

    private Integer id;
    private Integer mintage;
    private Double faceValue;
    private Double meltValue;
    private String currency;
    private String description;

    public CoinDTO() {
    }

    public CoinDTO(Integer id, Integer mintage, Double faceValue, Double meltValue,String currency,String description) {
        super();
        this.id = id;
        this.mintage = mintage;
        this.faceValue = faceValue;
        this.meltValue = meltValue;
        this.currency=currency;
        this.description=description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMintage() {
        return mintage;
    }

    public void setMintage(Integer mintage) {
        this.mintage = mintage;
    }

    public Double getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(double faceValue) {
        this.faceValue = faceValue;
    }

    public Double getMeltValue() {
        return meltValue;
    }

    public void setMeltValue(double meltValue) {
        this.meltValue = meltValue;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Builder {
        private Integer nestedId;
        private Integer nestedmintage;
        private Double nestedfaceValue;
        private Double nestedmeltValue;
        private String nestedcurrency;
        private String nesteddescription;

        public Builder id(int id) {
            this.nestedId = id;
            return this;
        }

        public Builder mintage(int mintage) {
            this.nestedmintage = mintage;
            return this;
        }

        public Builder faceValue(double faceValue) {
            this.nestedfaceValue = faceValue;
            return this;
        }

        public Builder meltValue(double meltValue) {
            this.nestedmeltValue = meltValue;
            return this;
        }

        public Builder currency (String currency){
            this.nestedcurrency=currency;
            return this;
        }
        public Builder description(String description){
            this.nesteddescription=description;
            return this;
        }

        public CoinDTO create() {
            return new CoinDTO(nestedId,  nestedmintage, nestedfaceValue,nestedmeltValue,nestedcurrency,nesteddescription );
        }

    }
}
