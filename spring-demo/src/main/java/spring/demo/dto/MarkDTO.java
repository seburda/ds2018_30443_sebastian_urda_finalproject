package spring.demo.dto;

import spring.demo.entities.User;

public class MarkDTO {

    private Integer id;
    private Integer giverId;
    private Integer takerId;
    private Double mark;

    public MarkDTO(){

    }

    public MarkDTO(Integer id, Integer giverId, Integer takerId, Double mark) {
        this.id = id;
        this.giverId = giverId;
        this.takerId = takerId;
        this.mark = mark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGiver() {
        return giverId;
    }

    public void setGiver(Integer giver) {
        this.giverId = giver;
    }

    public Integer getTaker() {
        return takerId;
    }

    public void setTaker(Integer taker) {
        this.takerId = taker;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    public static class Builder {
        private Integer nestedId;
        private Integer nestedGiverId;
        private Integer nestedTakerId;
        private Double nestedMark;

        public Builder id(int id){
            this.nestedId=id;
            return this;
        }

        public Builder giverId(Integer giver){
            this.nestedGiverId=giver;
            return this;
        }

        public Builder takerId(Integer taker){
            this.nestedTakerId=taker;
            return this;
        }

        public Builder mark(double mark){
            this.nestedMark=mark;
            return this;
        }
        public MarkDTO create(){
            return new MarkDTO(nestedId,nestedGiverId,nestedTakerId,nestedMark);
        }

    }
}
