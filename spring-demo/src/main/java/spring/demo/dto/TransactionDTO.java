package spring.demo.dto;

public class TransactionDTO {
    private Integer id;
    private Integer sellerId;
    private Integer buyerId;
    private Integer coinId;
    private Integer number;
    private Double price;

    public TransactionDTO() {
    }

    public TransactionDTO(Integer id, Integer sellerId, Integer buyerId, Integer coinId, Integer number, Double price) {
        this.id = id;
        this.sellerId = sellerId;
        this.buyerId = buyerId;
        this.coinId = coinId;
        this.number = number;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public Integer getCoinId() {
        return coinId;
    }

    public void setCoinId(Integer coinId) {
        this.coinId = coinId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public static class Builder{
        private Integer nestedId;
        private Integer nestedSellerId;
        private Integer nestedBuyerId;
        private Integer nestedCoinId;
        private Integer nestedNumber;
        private Double nestedPrice;

        public Builder id(int id){
            this.nestedId=id;
            return this;
        }
        public Builder sellerId(int id){
            this.nestedSellerId=id;
            return this;
        }
        public Builder buyerId(int id){
            this.nestedBuyerId=id;
            return this;
        }
        public Builder coinId(int id){
            this.nestedCoinId=id;
            return this;
        }
        public  Builder number(int number){
            this.nestedNumber=number;
            return this;
        }
        public Builder price(double price){
            this.nestedPrice=price;
            return this;
        }
        public TransactionDTO create(){
            return new TransactionDTO(nestedId,nestedSellerId,nestedBuyerId,nestedCoinId,nestedNumber,nestedPrice);
        }
    }

}
