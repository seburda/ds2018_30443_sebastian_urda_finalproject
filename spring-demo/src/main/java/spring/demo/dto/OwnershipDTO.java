package spring.demo.dto;

import spring.demo.entities.Coin;
import spring.demo.entities.User;

public class OwnershipDTO {
    private Integer id;
    private Integer personId;
    private Integer coinId;
    private Integer number;
    private Boolean visibility;
    private Boolean forSale;
    public OwnershipDTO() {
    }

    public OwnershipDTO(Integer id, Integer person, Integer coin, Integer number,boolean visible,boolean forSale) {
        super();
        this.id = id;
        this.personId = person;
        this.coinId = coin;
        this.number = number;
        this.visibility=visible;
        this.forSale=forSale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getCoinId() {
        return coinId;
    }

    public void setCoinId(Integer coinId) {
        this.coinId = coinId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public static class Builder{
        private Integer nestedid;
        private Integer nestedperson;
        private Integer nestedcoin;
        private Integer nestednumber;
        private boolean nestedvisibility;
        private boolean nestedForSale;

        public Builder id(int id){
            this.nestedid=id;
            return this;
        }

        public Builder personId(Integer personId){
            this.nestedperson=personId;
            return this;
        }

        public Builder coinId(Integer coinId){
            this.nestedcoin=coinId;
            return this;
        }

        public Builder number(int number){
            this.nestednumber=number;
            return this;
        }

        public Builder visible (boolean visible){
            this.nestedvisibility=visible;
            return this;
        }

        public Builder forSale(boolean forSale){
            this.nestedForSale=forSale;
            return this;
        }

        public OwnershipDTO create(){
            return new OwnershipDTO(nestedid,nestedperson,nestedcoin,nestednumber,nestedvisibility,nestedForSale);
        }
    }
}
