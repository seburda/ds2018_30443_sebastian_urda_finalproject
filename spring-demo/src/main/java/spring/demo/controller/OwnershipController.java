package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.OwnershipDTO;
import spring.demo.services.OwnershipService;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/owner")
public class OwnershipController {

    @Autowired
    private OwnershipService ownershipService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<OwnershipDTO> getAllOwners() {
        List<OwnershipDTO> owners=ownershipService.findAll();
        for(OwnershipDTO owner:owners)
        {
            System.out.println(owner.getId());
            System.out.println(owner.getPersonId());
        }
        return owners;
    }

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public List<OwnershipDTO> getAllOfUser(@PathVariable("id") int id) {
        return ownershipService.findAllByOwnerId(id);
    }

    @RequestMapping(value = "/details/{id}/{vis}", method = RequestMethod.GET)
    public List<OwnershipDTO> getAllVisibleOfUser(@PathVariable("id") int id,@PathVariable("vis") boolean vis) {
        return ownershipService.findAllByOwnerVisible(id,vis);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertOwnership(@RequestBody OwnershipDTO dto) {
        return ownershipService.create(dto);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public boolean deleteOwnership(@RequestBody OwnershipDTO dto) {
        return ownershipService.delete(dto);
    }
}
