package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.CoinDTO;
import spring.demo.services.CoinService;

import java.util.List;

@CrossOrigin(maxAge=3600)
@RestController
@RequestMapping("/coin")
public class CoinController {

    @Autowired
    private CoinService coinService;

    @RequestMapping(value="/details/{id}",method=RequestMethod.GET)
    public CoinDTO getCoinById(@PathVariable("id") int id){
        return coinService.findUserById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<CoinDTO> getAllCoins() {
        return coinService.findAll();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertCoin(@RequestBody CoinDTO coinDTO) {
        return coinService.create(coinDTO);
    }
}
