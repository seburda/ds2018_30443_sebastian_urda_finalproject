package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.TransactionDTO;
import spring.demo.services.TransactionService;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<TransactionDTO> getAllTransactions() {
        return transactionService.findAll();
    }

    @RequestMapping(value = "/buyer/{id}", method = RequestMethod.GET)
    public List<TransactionDTO> getTransactionByBuyerId(@PathVariable("id") int id) {
        return transactionService.findAllBuyerId(id);
    }
    @RequestMapping(value = "/seller/{id}", method = RequestMethod.GET)
    public List<TransactionDTO> getTransactionBySellerId(@PathVariable("id") int id) {
        return transactionService.findAllSellerId(id);
    }

    @RequestMapping(value = "/coin/{id}", method = RequestMethod.GET)
    public List<TransactionDTO> getTransactionByCoinId(@PathVariable("id") int id) {
        return transactionService.findAllCoinId(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertTransaction(@RequestBody TransactionDTO userDTO) {
        return transactionService.create(userDTO);
    }

    @RequestMapping(value="/delete",method=RequestMethod.DELETE)
    public boolean delete(@RequestBody TransactionDTO transactionDTO ) {
        return transactionService.delete(transactionDTO);
    }
}
