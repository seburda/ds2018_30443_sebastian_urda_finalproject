package spring.demo.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.demo.dto.MarkDTO;
import spring.demo.services.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/mark")
public class MarkController {

    @Autowired
    private MarkService markService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<MarkDTO> getAllMarks() {
        return markService.findAll();
    }

    @RequestMapping(value = "/taker/{id}", method = RequestMethod.GET)
    public List<MarkDTO> getAllOfTaker(@PathVariable("id") int id) {
        return markService.findAllOfTaker(id);
    }
    @RequestMapping(value = "/taker/average/{id}", method = RequestMethod.GET)
    public double getAverageMark(@PathVariable("id") int id) {
        return markService.averageMark(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertMark(@RequestBody MarkDTO dto) {
        return markService.create(dto);
    }



}
