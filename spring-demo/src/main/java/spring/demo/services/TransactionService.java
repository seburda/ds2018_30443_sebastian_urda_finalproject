package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.TransactionDTO;
import spring.demo.entities.Coin;
import spring.demo.entities.Ownership;
import spring.demo.entities.Transaction;
import spring.demo.entities.User;
import spring.demo.repositories.CoinRepository;
import spring.demo.repositories.OwnershipRepository;
import spring.demo.repositories.TransactionRepository;
import spring.demo.repositories.UserRepository;


import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private OwnershipRepository ownershipRepository;

    public List<TransactionDTO> findAll(){
        List<Transaction> transactions=transactionRepository.findAll();
        List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
        for(Transaction tr:transactions){
            TransactionDTO dto=new  TransactionDTO.Builder()
                                .id(tr.getId())
                                .sellerId(tr.getSeller().getId())
                                .buyerId(tr.getBuyer().getId())
                                .coinId(tr.getCoin().getId())
                                .number(tr.getNumber())
                                .price(tr.getPrice()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }
    public TransactionDTO findById(int id){
        Transaction transaction=transactionRepository.findById(id);
        TransactionDTO dto=new TransactionDTO.Builder()
                                    .id(transaction.getId())
                                    .buyerId(transaction.getBuyer().getId())
                                    .sellerId(transaction.getSeller().getId())
                                    .coinId(transaction.getCoin().getId())
                                    .number(transaction.getNumber())
                                    .price(transaction.getPrice())
                                    .create();
        return dto;
    }
    public List<TransactionDTO> findAllBuyerId(int id){
        List<Transaction> transactions=transactionRepository.findByBuyer_Id(id);
        List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
        for(Transaction tr:transactions){
            TransactionDTO dto=new  TransactionDTO.Builder()
                    .id(tr.getId())
                    .sellerId(tr.getSeller().getId())
                    .buyerId(tr.getBuyer().getId())
                    .coinId(tr.getCoin().getId())
                    .number(tr.getNumber())
                    .price(tr.getPrice()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }
    public List<TransactionDTO> findAllSellerId(int id){
        List<Transaction> transactions=transactionRepository.findBySeller_Id(id);
        List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
        for(Transaction tr:transactions){
            TransactionDTO dto=new  TransactionDTO.Builder()
                    .id(tr.getId())
                    .sellerId(tr.getSeller().getId())
                    .buyerId(tr.getBuyer().getId())
                    .coinId(tr.getCoin().getId())
                    .number(tr.getNumber())
                    .price(tr.getPrice()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }
    public List<TransactionDTO> findAllCoinId(int id){
        List<Transaction> transactions=transactionRepository.findByCoin_Id(id);
        List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
        for(Transaction tr:transactions){
            TransactionDTO dto=new  TransactionDTO.Builder()
                    .id(tr.getId())
                    .sellerId(tr.getSeller().getId())
                    .buyerId(tr.getBuyer().getId())
                    .coinId(tr.getCoin().getId())
                    .number(tr.getNumber())
                    .price(tr.getPrice()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public boolean delete (TransactionDTO dto){
        Transaction transaction=new Transaction();
        User seller= userRepository.findById(dto.getSellerId());
        User buyer=userRepository.findById(dto.getBuyerId());
        Coin coin=coinRepository.findById(dto.getCoinId());
        transaction.setId(dto.getId());
        transaction.setBuyer(buyer);
        transaction.setSeller(seller);
        transaction.setCoin(coin);
        transaction.setNumber(dto.getNumber());
        transaction.setPrice(dto.getPrice());
        transactionRepository.delete(transaction);
       transaction= transactionRepository.findById(dto.getId());
       if(transaction==null)
           return true;
       else return false;
    }
    public int create (TransactionDTO dto){
        Transaction transaction=new Transaction();
        User seller= userRepository.findById(dto.getSellerId());
        User buyer=userRepository.findById(dto.getBuyerId());
        Coin coin=coinRepository.findById(dto.getCoinId());
        Ownership ownership = ownershipRepository.findByOwner_IdAndCoin_Id(seller.getId(), coin.getId());
        if(ownership==null)
            return -1;
        System.out.println(seller.getName()+" "+ownership.getOwner().getName()+" "+buyer.getName()+" "+coin.getFaceValue());
        if(ownership.getNumber()==dto.getNumber()) {
            ownershipRepository.delete(ownership);
            Ownership tempOwnership = ownershipRepository.findByOwner_IdAndCoin_Id(buyer.getId(), coin.getId());
            if(tempOwnership!=null){
                tempOwnership.setNumber(tempOwnership.getNumber()+dto.getNumber());
                ownershipRepository.save(tempOwnership);
            }
            else {
                Ownership newOwnership = new Ownership();
                newOwnership.setOwner(buyer);
                newOwnership.setCoin(coin);
                newOwnership.setVisible(true);
                newOwnership.setForSale(false);
                newOwnership.setNumber(dto.getNumber());
                ownershipRepository.save(newOwnership);
            }
        }
        else {
            ownership.setNumber(ownership.getNumber() - dto.getNumber());
            ownershipRepository.save(ownership);
            Ownership tempOwnership = ownershipRepository.findByOwner_IdAndCoin_Id(buyer.getId(), coin.getId());
            if(tempOwnership!=null){
                tempOwnership.setNumber(tempOwnership.getNumber()+dto.getNumber());
                ownershipRepository.save(tempOwnership);
            }
            else {
                Ownership newOwnership = new Ownership();
                newOwnership.setOwner(buyer);
                newOwnership.setCoin(coin);
                newOwnership.setVisible(true);
                newOwnership.setForSale(false);
                newOwnership.setNumber(dto.getNumber());
                ownershipRepository.save(newOwnership);
            }
        }
        transaction.setBuyer(buyer);
        transaction.setSeller(seller);
        transaction.setCoin(coin);
        transaction.setNumber(dto.getNumber());
        transaction.setPrice(dto.getPrice());
        Transaction tr=transactionRepository.save(transaction);
        return tr.getId();
    }
}
