package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.dto.UserDTO;
import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;

@Service
public class UserService {
	private static final String SPLIT_CH = " ";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@Autowired
	private UserRepository usrRepository;

	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}

		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.name(usr.getName())
						.email(usr.getEmail())
						.password(usr.getPassword())
						.role(usr.getRole()).create();
		return dto;
	}

	public UserDTO findByName(String name)
	{
		User usr = usrRepository.findByName(name);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}

		UserDTO dto = new UserDTO.Builder()
				.id(usr.getId())
				.name(usr.getName())
				.email(usr.getEmail())
				.password(usr.getPassword())
				.role(usr.getRole()).create();
		return dto;
	}
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {

			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.name(user.getName())
						.email(user.getEmail())
						.password(user.getPassword())
						.role(user.getRole())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setName(userDTO.getName());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setRole(userDTO.getRole());

		User usr = usrRepository.save(user);
		return usr.getId();
	}
	public String findByUsernameAndPassword(UserDTO user){
		User found=usrRepository.findByNameAndPassword(user.getName(),user.getPassword());
		if(found==null)
			return "not found";
		else return found.getRole();
	}

	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getName() == null || "".equals(usr.getName())) {
			validationErrors.add("Name field should not be empty");
		}

		if (usr.getPassword() == null || "".equals(usr.getPassword())) {
			validationErrors.add("Password field should not be empty");
		}

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}

		return validationErrors;
	}

	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}
}
