package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.MarkDTO;
import spring.demo.entities.Mark;
import spring.demo.entities.User;
import spring.demo.repositories.MarkRepository;
import spring.demo.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class MarkService {

    @Autowired
    private MarkRepository markRepository;

    @Autowired
    private UserRepository userRepository;

    public List<MarkDTO> findAll(){
        List<Mark> marks=markRepository.findAll();
        List<MarkDTO> toReturn = new ArrayList<MarkDTO>();
        for(Mark mark: marks){
            MarkDTO dto=new MarkDTO.Builder()
                            .id(mark.getId())
                            .giverId(mark.getGiver().getId())
                            .takerId(mark.getTaker().getId())
                            .mark(mark.getMark()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public List<MarkDTO> findAllOfTaker(int id){
        List<Mark> marks=markRepository.findByTaker_Id(id);
        List<MarkDTO> toReturn = new ArrayList<MarkDTO>();
        for(Mark mark: marks){
            MarkDTO dto=new MarkDTO.Builder()
                    .id(mark.getId())
                    .giverId(mark.getGiver().getId())
                    .takerId(mark.getTaker().getId())
                    .mark(mark.getMark()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }

    public int create(MarkDTO dto){
        Mark mark=new Mark();
        User giver= userRepository.findById(dto.getGiver());
        mark.setGiver(giver);
        User taker=userRepository.findById(dto.getTaker());
        mark.setTaker(taker);
        mark.setMark(dto.getMark());

        Mark mrk=markRepository.save(mark);

        return mrk.getId();
    }
    public double averageMark(int id){
        List<Mark> marks=markRepository.findByTaker_Id(id);
        double sum=0;
        for(Mark mark: marks){
            sum=sum+mark.getMark();
        }
        return sum/marks.size();

    }


}
