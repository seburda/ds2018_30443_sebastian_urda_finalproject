package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.OwnershipDTO;
import spring.demo.entities.Coin;
import spring.demo.entities.Ownership;
import spring.demo.entities.User;
import spring.demo.repositories.CoinRepository;
import spring.demo.repositories.OwnershipRepository;
import spring.demo.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class OwnershipService {

    @Autowired
    private OwnershipRepository ownershipRepository;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private UserRepository userRepository;

    public List<OwnershipDTO> findAll() {
        List<Ownership> owners = ownershipRepository.findAll();
        List<OwnershipDTO> toReturn = new ArrayList<OwnershipDTO>();
        for (Ownership own : owners) {
            OwnershipDTO dto = new OwnershipDTO.Builder()
                    .id(own.getId())
                    .personId(own.getOwner().getId())
                    .coinId(own.getCoin().getId())
                    .number(own.getNumber())
                    .forSale(own.getForSale())
                    .visible(own.getVisible()).create();

            toReturn.add(dto);
        }
        return toReturn;
    }

    public List<OwnershipDTO> findAllByOwnerId(int id) {
        List<Ownership> owners = ownershipRepository.findByOwner_Id(id);
        List<OwnershipDTO> toReturn = new ArrayList<OwnershipDTO>();
        for (Ownership own : owners) {
            OwnershipDTO dto = new OwnershipDTO.Builder()
                    .id(own.getId())
                    .personId(own.getOwner().getId())
                    .coinId(own.getCoin().getId())
                    .number(own.getNumber())
                    .forSale(own.getForSale())
                    .visible(own.getVisible()).create();

            toReturn.add(dto);
        }
        return toReturn;
    }
    public List<OwnershipDTO> findAllByOwnerVisible(int id,boolean bool){

        List<Ownership> owners = ownershipRepository.findByOwner_IdAndAndVisible(id,bool);
        List<OwnershipDTO> toReturn = new ArrayList<OwnershipDTO>();
        for (Ownership own : owners) {
            OwnershipDTO dto = new OwnershipDTO.Builder()
                    .id(own.getId())
                    .personId(own.getOwner().getId())
                    .coinId(own.getCoin().getId())
                    .number(own.getNumber())
                    .forSale(own.getForSale())
                    .visible(own.getVisible()).create();

            toReturn.add(dto);
        }
        return toReturn;

    }
    public int create (OwnershipDTO dto){
        Ownership ownership=new Ownership();
        User owner =userRepository.findById(dto.getPersonId());
        ownership.setOwner(owner);
        Coin  coin=coinRepository.findById(dto.getCoinId());
        ownership.setCoin(coin);
        ownership.setNumber(dto.getNumber());
        ownership.setForSale(dto.isForSale());
        ownership.setVisible(dto.isVisibility());
        System.out.println(dto.getPersonId()+" "+dto.getCoinId()+" "+dto.isForSale());
        Ownership own=ownershipRepository.save(ownership);
        return own.getId();
    }
    public boolean delete(OwnershipDTO dto)
    {
        Ownership ownership=new Ownership();
        ownership.setId(dto.getId());
        User owner =userRepository.findById(dto.getPersonId());
        ownership.setOwner(owner);
        Coin  coin=coinRepository.findById(dto.getCoinId());
        ownership.setCoin(coin);
        ownership.setNumber(dto.getNumber());
        ownership.setForSale(dto.isForSale());
        ownership.setVisible(dto.isVisibility());
        ownershipRepository.delete(ownership);
        ownership=ownershipRepository.findById(dto.getId());
        if(ownership==null)
            return true;
        else return false;
    }
}
