package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.CoinDTO;
import spring.demo.entities.Coin;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.CoinRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CoinService {

    @Autowired
    private CoinRepository coinRepository;

    public CoinDTO findUserById(int coinId){
        Coin coin =coinRepository.findById(coinId);
        if(coin==null){
            throw new ResourceNotFoundException(Coin.class.getSimpleName());
        }
        CoinDTO dto= new CoinDTO.Builder()
                            .id(coin.getId())
                            .mintage(coin.getMintage())
                            .faceValue(coin.getFaceValue())
                            .meltValue(coin.getMeltValue())
                            .currency(coin.getCurrency())
                            .description(coin.getDescription()).create();
        return dto;
    }
    public List<CoinDTO> findAll(){
        List<Coin> coins=coinRepository.findAll();
        List<CoinDTO> toReturn= new ArrayList<CoinDTO>();
        for(Coin coin:coins){
            CoinDTO dto=new CoinDTO.Builder()
                            .id(coin.getId())
                            .mintage(coin.getMintage())
                            .faceValue(coin.getFaceValue())
                            .meltValue(coin.getMeltValue())
                            .currency(coin.getCurrency())
                            .description(coin.getDescription()).create();
            toReturn.add(dto);
        }
        return toReturn;
    }
    public int create(CoinDTO coinDTO) {
        Coin coin=new Coin();
        coin.setFaceValue(coinDTO.getFaceValue());
        coin.setMeltValue(coinDTO.getMeltValue());
        coin.setMintage(coinDTO.getMintage());
        coin.setDescription(coinDTO.getDescription());
        coin.setCurrency(coinDTO.getCurrency());
        Coin c=coinRepository.save(coin);
        return c.getId();
    }

}
